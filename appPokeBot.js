require('dotenv').config()
const { Telegraf } = require('telegraf')
const axios = require('axios')

//Inicializamos una instancia de telegraf
const bot = new Telegraf(process.env.BOT_TOKEN)

//VARIABLES
const help = 'Usa el comando /pokemon y el nombre para buscar info de tu pokemon favorito\
                \nUsa el comando /pokemonrandom y te daré info de un pokemon cualquiera\
                \nExitos!!'

//FUNCIONES
//Funcion que genera un numero ramdom
function random(number) {
    return Math.floor(Math.random() * (number + 1))
}
//Funcion que me develve info de un pokemon en si
async function fetchPokemon(pokemon) {
    try {
        const res = await axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon}`)
        return res.data
    } catch (error) {
        return "Not Found"
    }
}

//Funcion que me devuelve un pokemon al azar
async function fetchPokemonRandom(pokemon) {
    const res = await axios.get(`https://pokeapi.co/api/v2/pokemon/${random(898)}`)
    return res.data
}
//Funcion que envia un mensaje de bienvenida al usuario
function sendStartMessage(resp) {
    const startMessage = `Bienvenido ${resp.message.chat.first_name}!!`
    bot.telegram.sendMessage(resp.chat.id, startMessage, {
        reply_markup: {
            inline_keyboard: [
                [
                    { text: "Info", callback_data: "info" }
                ],
                [
                    { text: "Website", url: "https://www.tsoftglobal.com" }
                ],
                [
                    { text: "Creditos", callback_data: 'credits' }
                ]
            ]
        }
    })
}

//COMANDOS
//Espera por el comando start
bot.start((ctx) => {
    //ctx.reply(`Bienvenido ${ctx.message.chat.first_name} !`)
    sendStartMessage(ctx)
})
//Comando que muestra info del proyecto
bot.action('info', (ctx) => {
    ctx.reply(help)
})
//Comando que muestra quien es el autor
bot.action('credits', resp => {
    resp.answerCbQuery()
    resp.reply('Creado por Daniel Barros para Tsoft como DEMO de un ChatBot, busco info de pokemons consumiendo una API externa!')
})

//Espera por el comando help
bot.help((ctx) => {
    ctx.reply(help)
})
//Comando de pruebas
bot.command('random', (ctx) => {
    ctx.reply(random(100))
})
bot.command('advancerandom', (ctx) => {
    const message = ctx.message.text
    const randomNamber = Number(message.split(' ')[1])

    if (isNaN(randomNamber) || randomNamber <= 0) {
        ctx.reply('Por favor ingresa /advancerandom seguido de un numero mayor que 0, Ej: /advancerandom 20')
    } else {
        ctx.reply(random(randomNamber))
    }
})

//comando que permite buscar un pokemon especifico
bot.command('pokemon', async (ctx) => {
    const message = ctx.message.text
    const pkm = message.split(' ')[1].toLowerCase()
    console.log(`${ctx.message.chat.first_name} esta haciendo peticiones`)
    const pkmRes = await fetchPokemon(pkm)
    if (pkmRes != "Not Found") {
        bot.telegram.sendPhoto(ctx.message.chat.id, pkmRes.sprites.front_default)
        setTimeout(() => {
            ctx.reply(`>>${pkmRes.name}<<\n\nId: ${pkmRes.id}\nAltura: ${(pkmRes.height) / 10} m\nPeso: ${(pkmRes.weight) / 10} Kg`)
        }, 1000)
        setTimeout(() => {
            ctx.reply(`HP: ${pkmRes.stats[0].base_stat}\nAtaque: ${pkmRes.stats[1].base_stat}\
                \nDefensa: ${pkmRes.stats[2].base_stat}\
                \nAtaque Especial: ${pkmRes.stats[3].base_stat}\
                \nDefensa Especial: ${pkmRes.stats[4].base_stat}\
                \nVelocidad: ${pkmRes.stats[5].base_stat}`)
        }, 1000);
    } else {
        ctx.reply(`No existe un pokemon llamado "${pkm}", intenta con otro nombre`)
    }
})

//comando que devuelve un pokemon random
bot.command('pokemonrandom', async (ctx) => {
    console.log(`${ctx.message.chat.first_name} esta haciendo peticiones`)
    const pkmRes = await fetchPokemonRandom()
    bot.telegram.sendPhoto(ctx.message.chat.id, pkmRes.sprites.front_default)
    setTimeout(()=>{
        ctx.reply(`>>${pkmRes.name}<<\n\nId: ${pkmRes.id}\nAltura: ${(pkmRes.height) / 10} m\nPeso: ${(pkmRes.weight) / 10} Kg`)
    })
    setTimeout(function () {
        ctx.reply(`HP: ${pkmRes.stats[0].base_stat}\nAtaque: ${pkmRes.stats[1].base_stat}\
                \nDefensa: ${pkmRes.stats[2].base_stat}\
                \nAtaque Especial: ${pkmRes.stats[3].base_stat}\
                \nDefensa Especial: ${pkmRes.stats[4].base_stat}\
                \nVelocidad: ${pkmRes.stats[5].base_stat}`)
    }, 1000);
})



//lanzamos el bot
bot.launch()